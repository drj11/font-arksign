# Evaluation of Arksign

2023-03-13

First complete alphabet.

The alphabet is made mostly following a regular upright roman form.

Strokes are even with a modest about of thick/thin modulation.
There is a flare to the ends, sometimes gently and mostly linear,
other times more rapid and irregular.

Across the alphabet some letterforms are fairly mainstream,
others more quirky.

The wild and surprising choice of widths for the letters is
a characterful feature of the font that i would recommend keeping.
The /H is quite narrow, the /A quite wide.
The /O has pleasing circular proportions.

Many letterforms have a distinct "not symmetric" feel to their
thick/thin placement. Again, keep this. In fact, emphasise it more
in some letters (/S /N).

Overall weight and value needs adjust to make more uniform
but take care not to destroy all character.
Consider correcting overall grey value by increasing thick/thin
contrast, and/or try unusual placements.

Vertical metrics are somewhat uneven. the /M and /Y project above
the cap line considerably, the /R sits way above the baseline.
The /Z is tiny and fails to reach the capline.
I recommended that the most excessive cases are corrected, but
take care to keep some of the uneven nature.

## Specifically vertically:

- Let /M project above capline.
- Extend /R to reach baseline, consider moving leg
  so that it goes below baseline.
- Extend /S slightly so that the middle of the stroke ending
  is on the horizontal cap-/base-line
- from /S, copy stroke-end treatment to /K /V /W /X
- Let /Y project above capline (it's funny)
- Let /Z stay below capline? by approximately one stroke thickness.

## Specifically horizontally:

- narrowen /D ?
- narrowen /E /F ?
- narrowen /G ?
- narrown /I /J ?
- Consider wider alts?


# END
