# arksign — A Rudolf Koch Sign

Letters made from shapes found in Rudolf Koch‘s « A Book of
Signs ».

![Alphabet](arksign-plaque-uc.svg)

# END
